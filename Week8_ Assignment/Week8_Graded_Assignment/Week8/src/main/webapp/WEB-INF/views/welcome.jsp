<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="java.util.List"%>
<%@ page import="com.spring.model.*"%>

<!DOCTYPE html>

<html>

<head>

<meta charset="ISO-8859-1">

<title>Profile Page</title>


</head>

<body>
		<%
		String username = (String) request.getAttribute("username");
		session.setAttribute("username", username);
		%>
		
		<h1 style="font-size: 80px">Welcome To Library</h1>
		<h1>
			Hello
			<%=username%></h1>
		<p>
			<a href="viewReadLater" style="font-size: 30px">Read Later</a>
		</p>
		<p>
			<a href="viewLike" style="font-size: 30px">Like</a>
		</p>
		
			<form method="post" action="saveBooks">
				<table border="1" style="background-color: #BDB76B">
					<tr>
						<td>Book_ID</td>
						<td>Book_Name</td>
						<td>Total_Pages</td>
						<td>Rating</td>
						<td>ISBN</td>
						<td>Read_Later</td>
						<td>Loved_Book</td>
					</tr>


					<%
					@SuppressWarnings("unchecked")
					List<Book> book = (List<Book>) request.getAttribute("list");
					%>

					<%
					for (Book b : book) {
					%>

					<tr>
						<td><%=b.getBook_id()%></td>
						<td><%=b.getTitle()%></td>
						<td><%=b.getTotal_pages()%></td>
						<td><%=b.getRating()%></td>
						<td><%=b.getIsbn()%></td>
						<td><a
							href="#"><button
									type="button">Read Later</button></a></td>
						<td><a
							href="#"><button
									type="button">Like</button></a></td>
					</tr>
					<%
					}
					%>
				</table>
			</form>
</body>
</html>