package com.spring.model;

public class Book {

	int book_id;
	String title;
	int total_pages;
	float rating;
	int isbn;

	public Book() {

	}

	public Book(int book_id, String title, int total_pages, float rating, int isbn) {
		this.book_id = book_id;
		this.title = title;
		this.total_pages = total_pages;
		this.rating = rating;
		this.isbn = isbn;
	}

	public int getBook_id() {
		return book_id;
	}

	public void setBook_id(int book_id) {
		this.book_id = book_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getTotal_pages() {
		return total_pages;
	}

	public void setTotal_pages(int total_pages) {
		this.total_pages = total_pages;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	public int getIsbn() {
		return isbn;
	}

	public void setIsbn(int isbn) {
		this.isbn = isbn;
	}

}
