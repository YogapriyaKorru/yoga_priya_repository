create database  Bookess;
use Bookess;


create table books(
  book_id        int not null, 
  title          varchar(255) not null, 
  total_pages    int not null, 
  rating         decimal(4, 2)not null, 
  isbn           int not null, 
  primary key(book_id)
);


insert into books values(1,"Wings of fire",219,4.5,149115),
(2,"A Thousand Splendid Suns",378,4.5,450451),
(3,"Life Is What You Make It",97,3.5,415451),
(4,"Animal Farm",918,4.6,146051),
(5,"The Laws of Human Nature",454,4.1,152351),
(6,"Strong Women",1256,4.2,223451),
(7,"The Murdered House A Mystery",1352.80,4.3,111481),
(8,"Truly Devious",296,4.4,456751),
(9,"Death By Coffee",785,4.6,145189),
(10,"Beautiful Joe",1070,4.5,454451),
(11,"Bhitti",553,4.7,409851),
(12,"Indian Art and Culture",443,4.8,124515),
(13,"The Monk Who Sold His Ferrari",160,4.1,514501),
(14,"A Promised Land",1039,4.0,245371),
(15,"The Blue Umbrella",91,4.4,450861);

select * from books;

create table users (
   user_id 	   int not null auto_increment,
   username        varchar(45) not null,
   email 	   varchar(45) not null,
   password    varchar(45) not null,
   primary key(user_id)
);
insert into users values(1,"yogapriya","korruyogapriya@gmail.com","2108");

select * from users;
