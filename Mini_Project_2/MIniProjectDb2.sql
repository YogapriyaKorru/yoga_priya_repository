create database MiniProject;
Use MiniProject;

create table admin(
email varchar(20) primary key,
password varchar(20));

create table Order_table(
order_id      int not null auto_increment,
order_name        varchar(45) not null,
order_price  int,
   primary key(order_id)
);

create table User (
   id 	   int not null auto_increment,
   user_name        varchar(45) not null,
  password 	   varchar(45) not null,
   primary key(id)
);

create table Item(         
item_id 	   int not null auto_increment,
   item_name       varchar(255) ,
   item_price       int,
primary key(item_id)
);

 alter table Item 
       add column item_id integer not null,
       add column item_name varchar(255),
       add column item_price integer not null,
       add column order_id integer;

    
    alter table Order_table
       add column order_id integer not null,
       add column order_name varchar(255),
       add column order_price integer not null;

    alter table user 
       add column user_name varchar(255);


    alter table item 
       add constraint FKifecq4kxptn4b9bvahbsiyr36 
       foreign key (order_id) 
       references order_table (order_id);
    
    create table user_orders (
       user_id integer not null,
        order_id integer not null);

          alter table user_orders 
       add column user_id integer not null,
       add column order_id integer not null;


 alter table user_orders 
       add constraint FKkuspr37yv513ga1okogyxrb7m 
       foreign key (user_id) 
       references user (id);

    alter table user_orders 
       add constraint FKfgftkgiabp9am6xtkq9tr898u 
       foreign key (order_id) 
       references Order_table (order_id);

 
 




    
   
  