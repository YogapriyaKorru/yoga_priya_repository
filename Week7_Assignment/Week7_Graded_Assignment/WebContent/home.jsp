<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home Page</title>
</head>
<body>
<div class="container">
		<div style="text-align:center;font-size:40px"> All the Books</div>
		<div class="row">
		
		<style>
		.Priya{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.Raj{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.Reddy{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.Sudha{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.Durga{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.Sunny{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
</style>
		
		<div class="Priya">
		<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/life secrets.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:100</h5>
						<h5 class="booktitle" >BookTitle:"Life Secrets"</h5>
						<h6 class="bookgenre">Bookgenre:"Motivational"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="add-to-favourite?id=2"><button>Add to Cart</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="Raj">
		
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/Finding Wonders.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:200</h5>
						<h5 class="booktitle" >BookTitle:"Finding Wonders"</h5>
						<h6 class="bookgenre">Bookgenre:"Inspirational"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="add-to-favourite?id=1"><button>Add to Cart</button></a> 
						</div>
					</div>
				</div>	
			</div>	
		</div>	
			
			<div class="Reddy">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/minions.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:300</h5>
						<h5 class="booktitle" >BookTitle:"Minions"</h5>
						<h6 class="bookgenre">Bookgenre:"Cartoon"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="add-to-favourite?id=2"><button>Add to Cart</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="Sudha">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/The Women in Black.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:400</h5>
						<h5 class="booktitle" >BookTitle:"The Women in Black "</h5>
						<h6 class="bookgenre">Bookgenre:"Horror"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="add-to-favourite?id=2"><button>Add to Cart</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="Durga">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/What in God's Name.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:500</h5>
						<h5 class="booktitle" >BookTitle:"What in God's Name"</h5>
						<h6 class="bookgenre">Bookgenre:"Comedy"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="add-to-favourite?id=2"><button>Add to Cart</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="Sunny">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/Ancient and Medieval India.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:600</h5>
						<h5 class="booktitle" >BookTitle:"Ancient and Medieval India"</h5>
						<h6 class="bookgenre">Bookgenre:"History""</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="add-to-favourite?id=2"><button>Add to Cart</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			
		</div>
	</div>

</body>
</html>