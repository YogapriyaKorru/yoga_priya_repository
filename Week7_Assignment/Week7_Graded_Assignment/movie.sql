create database hello;
use hello;

create table login(username varchar(10) not null,password varchar(10) not null);
insert into login values("yogapriya",2108);
insert into login values("raj",123);
select * from login;

create table registration(username varchar(10) not null,password varchar(10) not null,email varchar(10));
insert into registration values("yogapriya",2108,yogapriya@gmail.com);
insert into registration values("raj",123,raj@gmail.com);

select * from registration;

create table books(bookid int primary key,booktitle varchar(10),bookgenre varchar(10),image varchar(20));
insert into books values(100,"Life Secrets","Motivational","life secrets.jpg");
insert into books values(200,"Finding Wonders","Inspirational","Finding Wonders.jpg");
insert into books values(300,"Minions"," Cartoon","minions.jpg");
insert into books values(400,"The Women in Black","Horror","The Women in Black.jpg");
insert into books values(500,"What in God's Name","Comedy","What in God's Name.jpg");
insert into books values(600,"Ancient and Medieval India"," History","Ancient and Medieval India.jpg");

select * from books;