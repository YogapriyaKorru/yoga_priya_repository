package com.miniproject.sr;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;

import com.miniproject.sr.pojo.AdminLogin;
import com.miniproject.sr.pojo.Items;
import com.miniproject.sr.pojo.LoginUser;
import com.miniproject.sr.repository.AdminLogRepository;
import com.miniproject.sr.repository.ItemRepository;
import com.miniproject.sr.repository.UserRepository;




@SpringBootApplication
public class MiniprojectApplication implements CommandLineRunner {

	public static void main(String[] args) {
		
		SpringApplication.run(MiniprojectApplication.class, args);
	}
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ItemRepository bookRepository;
	
	@Autowired
	private AdminLogRepository adminLogRepository;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	public void run() {
		
	}
	@Override
	public void run(String... args) throws Exception {
		String sql="set FOREIGN_KEY_CHECKS=0";
		int result=jdbcTemplate.update(sql);
	}
	
	@Bean
	public void inserData(){
		LoginUser login1=new LoginUser("priya","K","priya@gmail.com","9988633265","123","123");
		userRepository.save(login1);
		
		LoginUser login2=new LoginUser("vag","G","vag@gmail.com","8142088970","456","456");
		userRepository.save(login2);
		
		LoginUser login3=new LoginUser("pooji","Y","pooji@gmail.com","8897546740","345","345");
		userRepository.save(login3);
		
		LoginUser login4=new LoginUser("raj","K","raj@test.com","7799164969","897","897");
		userRepository.save(login4);
		
		AdminLogin admin1=new AdminLogin("sanju", "R", "sanju@gmail.com", "9121373793", "567", "567");
		adminLogRepository.save(admin1);

		Items book=new Items("Veg Roll","90","3","IMG001");
		bookRepository.save(book);
		
		Items book1=new Items("Burgur","120","1","IMG002");
		bookRepository.save(book1);
		
		Items book2=new Items("Samosa","25","1","IMG003");
		bookRepository.save(book2);
		
		Items book3=new Items("South India Meal ","160","3","IMG004");
		bookRepository.save(book3);
		
		Items book4=new Items("Parata","60","2","IMG005");
		bookRepository.save(book4);
		
		
	}


}
