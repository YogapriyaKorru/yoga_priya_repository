package com;
import java.util.*;

public class Main {
public static void main(String[] args) {
		
		ArrayList<EmployeeDetails> employees = new ArrayList<>();
		
		//Creating object of EmployeeDetails class  
        EmployeeDetails emp1 = new EmployeeDetails(); 
        EmployeeDetails emp2 = new EmployeeDetails(); 
        EmployeeDetails emp3 = new EmployeeDetails(); 
        EmployeeDetails emp4 = new EmployeeDetails(); 
        EmployeeDetails emp5 = new EmployeeDetails(); 
        
        //Setting values to the properties  
        emp1.setEmployeeDetails(1,"Aman",20,1100000,"IT","Delhi");
        emp2.setEmployeeDetails(2,"Bobby",22,500000,"HR","Bombay");
        emp3.setEmployeeDetails(3,"Zoe",20,750000,"Admin","Delhi");
        emp4.setEmployeeDetails(4,"Smitha",21,1000000,"IT","Chennai");
        emp5.setEmployeeDetails(5,"Smitha",24,1200000,"HR","Bengaluru");
       
        
        //Adding Object to ArrayList
        employees.add(emp1);
        employees.add(emp2);
        employees.add(emp3);
        employees.add(emp4);
        employees.add(emp5);
 
      //Showing Employee details  
        System.out.println("Display Employee Details:");

        emp1.getEmployeeDetails();
        emp2.getEmployeeDetails();
        emp3.getEmployeeDetails();
        emp4.getEmployeeDetails();
        emp5.getEmployeeDetails();
       
     	System.out.println("****************************************************************");
     	
		//Sort Employee Name
		EmployeeNameInSortedOrder ename = new EmployeeNameInSortedOrder();
		ename.sortingEmployeeNames(employees);
		System.out.println("***********************************************************************");
		
		//City Name Count
		EmployeeCountFromEachCity cityNameCount = new EmployeeCountFromEachCity();
		cityNameCount.employeeCountFromEachCity(employees);
		System.out.println("*****************************************************************************");
		
		//Showing Monthly Salary
		MonthlySalary monthlySalary= new MonthlySalary();
		monthlySalary.monthlySalary(employees);
		System.out.println("***************************************************************************");
	}
}



