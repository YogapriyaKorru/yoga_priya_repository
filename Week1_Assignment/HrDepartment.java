package com;
public class HrDepartment  extends SuperDepartment {
	//declare method departmentName of return type string
	public String departmentName() {
		return"HR DEPARTMENT";
	}
	//declare method getTodaysWork of return type string
	public String getTodaysWork() {
		return"FILL UR WORK SHEET AND MARK UR ATTENDANCE";
	}
	//declare method doActivity of return type string
	public String doActivity(){
		return"TEAM LAUNCH";
		
	}
	//declare method getWorkDeadline of return type string
	public String getWorkDeadline() {
		return"COMPLETE BY END OF DAY";
	}

}