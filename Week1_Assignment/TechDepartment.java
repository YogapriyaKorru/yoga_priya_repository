package com;

public class TechDepartment extends SuperDepartment {
	
	//declare method departmentName of return type string
	public String departmentName() {
		return" TECH DEPARTMENT";
	}
	//declare method getTodaysWork of return type string
	public String getTodaysWork() {
		return"COMPLETE CODING OF MODULE ONE";
	}
	//declare method getTechStackInformation of return type string
	public String getTechstackInformation() {
		return"CORE JAVA";
	}
	//declare method getWorkDeadline of return type string
	public String getWorkDeadline() {
		return"COMPLETE IT BY END OF DAY";
	}

}
