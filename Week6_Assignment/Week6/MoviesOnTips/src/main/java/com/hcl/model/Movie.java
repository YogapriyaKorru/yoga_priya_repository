package com.hcl.model;
public class Movie {
int id ;
String title;
int year;
String category;
public Movie() {}
public Movie(int id, String title, int year, String category) {
	this.id = id;
	this.title = title;
	this.year = year;
	this.category = category;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public int getYear() {
	return year;
}
public void setYear(int year) {
	this.year = year;
}
public String getCategory() {
	return category;
}
public void setCategory(String category) {
	this.category = category;
}
@Override
public String toString() {
	return "Movie [id=" + id + ", title=" + title + ", year=" + year + ", category=" + category + "]";
}
}
